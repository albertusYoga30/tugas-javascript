/**
 *
 * Diberikan function ubahHuruf(kata) yang akan menerima satu parameter berupa string.
 * Function akan me-return sebuah kata baru dimana setiap huruf akan digantikan dengan huruf alfabet setelahnya.
 * Contoh, huruf a akan menjadi b, c akan menjadi d, k menjadi l, dan z menjadi a.
 *
 */

function ubahHuruf(kata) {
  var alfabet = "abcdefghijklmnopqrstuvwxyz";
  var index = 0;
  var temp = "";
  for (let i = 0; i < kata.length; i++) {
    index = alfabet.indexOf(kata[i]);
    temp += alfabet[index + 1];
  }
  return temp;
}

// TEST CASES
console.log(ubahHuruf("wow")); // xpx
console.log(ubahHuruf("developer")); // efwfmpqfs
console.log(ubahHuruf("javascript")); // kbwbtdsjqu
console.log(ubahHuruf("keren")); // lfsfo
console.log(ubahHuruf("semangat")); // tfnbohbu
